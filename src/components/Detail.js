import React from 'react';

const Detail = ({title, description}) => {
    return (<div>
        <div>
            <label>Title: </label>
            <span>{title}</span>
        </div>
        <div>
            <label>description: </label>
            <span>{description}</span>
        </div>
    </div>)
};

export default Detail;