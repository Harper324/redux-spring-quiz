const initState = {
    productDetail: []
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'GET_PRODUCTS_DETAIL':
            return {

                ...state,
                productDetail: action.productDetail
            };

        default:
            return state;
    }
};
