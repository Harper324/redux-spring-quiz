import React, {Component} from 'react';

export default class CreateProduct extends Component {


    constructor(props) {
        super(props);
        this.state = {
            name: '',
            price: 0,
            unit: '',
            image: ''
        };

        this.addProduct = this.addProduct.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handlePrice = this.handlePrice.bind(this);
        this.handleUnit = this.handleUnit.bind(this);
        this.handleImage = this.handleImage.bind(this);
    }

    addProduct() {

        const data = this.state;
        console.log(data);
        const url = 'http://localhost:8080/api/products';
        const myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        fetch(url, {
            // credentials: 'include',
            //允许跨域访问
            mode:'cors',
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(data)
        })
            .catch(error => console.error('Error', error))
            .then(response => console.log('Success', response));
    }

    handleName(event) {
        this.setState({
            name: event.target.value,
        })
    };

    handlePrice(event) {
        this.setState({
            price: event.target.value
        })
    };

    handleUnit(event) {
        this.setState({
            unit: event.target.value
        })
    };

    handleImage(event) {
        this.setState({
            image: event.target.value
        })
    };

    render() {
        return (
            <div className='createProduct'>
                <form onSubmit={this.addProduct}>
                    <h1>添加商品</h1>
                    <h2>*名称</h2>
                    <input type="text" value={this.state.name} onChange={this.handleName}/>
                    <h2>*价格</h2>
                    <input type="number" value={this.state.price} onChange={this.handlePrice}/>
                    <h2>*单位</h2>
                    <input type="text" value={this.state.unit} onChange={this.handleUnit}/>
                    <h2>*图片</h2>
                    <input type="text" value={this.state.image} onChange={this.handleImage}/>
                    <button type="submit">提交</button>
                </form>

            </div>
        );
    }
}
