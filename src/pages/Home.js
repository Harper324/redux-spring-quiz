import React, {Component} from 'react';
import {connect} from "react-redux";
import Menu from "./Menu";
import {getProductDetail} from "../actions/productAction";
import "./Home.less";

class Home extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            // link: null
        }
    }

    componentDidMount() {
        this.props.getProductDetail();

    }

    render() {
        console.log(this.props.productDetail);
        return (
            <div className='home'>
                {this.props.productDetail.map((product, key) => (<Menu key={key} id={product.id} value={product}/>))}
                <Menu id='create' value='create'/>
            </div>
        );
    }
}

const mapStateToProps = state => (
    {
        productDetail: state.product.productDetail
    });

const mapDispatchToProps = dispatch => ({
    getProductDetail: () => dispatch(getProductDetail())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
