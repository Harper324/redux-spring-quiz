import React from "react";

const Menu = ({id, value}) => {
    return (
        <div className='home-menu'>
            <span>
                {value.name}
            </span>
            <span>
                {value.price}/{value.unit}
            </span>
        </div>
    );
};

export default Menu;