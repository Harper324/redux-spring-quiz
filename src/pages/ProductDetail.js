import React, {Component} from 'react';
import {connect} from "react-redux";
import {getProductDetail} from "../actions/productAction";
import Detail from "../components/Detail";

class ProductDetail extends Component {
    componentDidMount() {
        this.props.getProductDetail();

        // console.log(this.props.productDetail);
    }

    render() {
        let id1 = this.props.match.params.id;
        console.log(id1);
        const {title, description} = this.props.productDetail.find(product => product.id == id1);

        return (


            <div className="">
                <h3>Product Details Page</h3>
                <Detail title={title} description={description}/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    productDetail: state.product.productDetail
});


const mapDispatchToProps = dispatch => ({
    getProductDetail: () => dispatch(getProductDetail())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
