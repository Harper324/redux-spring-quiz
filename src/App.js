import React, {Component} from 'react';
import './App.less';
import Home from "./pages/Home";
import {BrowserRouter, Link, Route, Switch} from "react-router-dom";
import CreateProduct from "./pages/CreateProduct";
import Order from "./pages/Order";


class App extends Component {
    render() {
        return (

            <BrowserRouter>
                <header>
                    <div><Link to='/products'>商城</Link></div>
                    <div><Link to="/orders">订单</Link></div>
                    <div><Link to="/products/create">+添加商品</Link></div>

                </header>

                <Switch>

                    <Route path="/orders" component={Order}/>
                    <Route path="/products/create" component={CreateProduct}/>
                    <Route exact path="/products" component={Home}/>
                </Switch>

            </BrowserRouter>
        );
    }
}

export default App;