export const getProductDetail = () => (dispatch) => {

    fetch('http://localhost:8080/api/products')
        .then(response => response.json())
        .then(data => {
            dispatch(
                {
                    type: 'GET_PRODUCTS_DETAIL',
                    productDetail: data
                });
        })

};

export const updateOrder = (data) => (dispatch) => {

    fetch('http://localhost:8080/api/products', {
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json'
        },
        method: 'POST',
    }).then(response => response.json())
        .then(data => {
            dispatch(
                {
                    type: 'UPDATE_ORDER',
                    orderDetail: data
                }
            );
        })
};

export const deleteOrder = (data) => (dispatch) => {

    const url = 'http://localhost:8080/api/products' + data;
    fetch(url, {
        body: JSON.stringify(data),
        headers: {
            'content-type': 'text/plain'
        },
        method: 'DELETE',
    }).then(response => response.json())
        .then(data => {
            dispatch(
                {
                    type: 'DELETE_ORDER',
                    orderDetail: data
                }
            )
        })
};

// export const addProduct = (data) => (dispatch) => {
//
//     fetch('http://localhost:8080/api/products', {
//         body: JSON.stringify(data),
//         headers: {
//             'content-type': 'application/json'
//         },
//         method: 'POST',
//     }).then(response => response.json())
//         .then(data => {
//             dispatch(
//                 {
//                     type: 'ADD_PRODUCT',
//                     productDetail: data
//                 }
//             );
//         })
//
// };